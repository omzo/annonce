<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Livre extends Model 
{

    protected $table = 'livres';
    public $timestamps = true;

    public function auteur()
    {
        return $this->belongsTo('Auteur');
    }

}