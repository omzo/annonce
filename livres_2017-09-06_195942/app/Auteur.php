<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auteur extends Model 
{

    protected $table = 'auteurs';
    public $timestamps = true;
    protected $fillable = array('nom');

    public function livres()
    {
        return $this->hasMany('Livre');
    }

}