<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Route::get('/', 'UsersController@getInfos');
Route::post('users', 'UsersController@postInfos');
Route::get('contact', 'ContactController@getForm');
Route::post('contact', 'ContactController@postForm');
Route::get('photo/form', 'PhotoController@getForm');
Route::post('photo/form', 'PhotoController@postForm');
Route::get('email/form', 'EmailController@getForm');
Route::post('email/form', 'EmailController@postForm');
Route::resource('user', 'UserController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('post', 'PostController', ['except' => ['show', 'edit', 'update']]);
Route::resource('post', 'PostController', ['except' => ['show', 'edit', 'update']]);

Route::get('post/tag/{tag}', 'PostController@indexTag');

Route::resource('livre', 'LivreController');
Route::resource('auteur', 'AuteurController');
